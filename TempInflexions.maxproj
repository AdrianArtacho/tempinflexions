{
	"name" : "TempInflexions",
	"version" : 1,
	"creationdate" : 3727628445,
	"modificationdate" : 3727628633,
	"viewrect" : [ 25.0, 104.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"TempInflexions.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"tempi_sequence.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"tempi_inflexions.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"tempi_increment.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"tempi_bpm2speed.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"tempi_seek.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"itable_write.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"itable_read.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"tempi_midi2speed.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"tempi_speed2bpm.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"ModSquad_tests.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"transposer-poly.maxpat" : 			{
				"kind" : "patcher"
			}
,
			"transratio.maxpat" : 			{
				"kind" : "patcher"
			}

		}
,
		"media" : 		{
			"Dwellings_3.aiff" : 			{
				"kind" : "audiofile",
				"local" : 1
			}

		}
,
		"data" : 		{
			"tempo_inflexions.txt" : 			{
				"kind" : "textfile",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
